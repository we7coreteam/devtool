<?php
include 'vendor/autoload.php';
error_reporting(1);
ini_set('display_errors', 1);
$config = array();

function abc() {
	$a = debug_backtrace();
	var_dump($a);
	exit;
}
abc();


$config['db']['driver'] = 'mysql';
$config['db']['host'] = '172.16.1.12';
$config['db']['username'] = 'root';
$config['db']['password'] = '123456';
$config['db']['port'] = '3306';
$config['db']['database'] = 'we7_addons';
$config['db']['charset'] = 'utf8';
$config['db']['pconnect'] = 0;
$config['db']['tablepre'] = 'ims_';
$config['db']['prefix'] = 'ims_';

$container = \Illuminate\Container\Container::getInstance();
$container->singleton('events', function() use ($container) {
	return new \Illuminate\Events\Dispatcher($container);
});
$db_manager = new Illuminate\Database\Capsule\Manager($container);
$db_manager->addConnection($config['db']);
$db_manager->setEventDispatcher($container['events']);
$db_manager->bootEloquent();
$db_manager->setAsGlobal();

//\Illuminate\Database\Capsule\Manager::listen(function($sql){
//	var_dump($sql);
//	exit;
//});






$debugbar = \We7\Dev\DebugBar\We7DebugBar::instance(__DIR__.'/test/debugbar');
$debugbar->showOnShutdown();
\Illuminate\Database\Capsule\Manager::table('members')->limit(5)->get();


$html = <<<EOT
	<html>
	<head>

	</head>

	<body>
		hello world
	</body>
</html>
EOT;
echo $html;

?>

