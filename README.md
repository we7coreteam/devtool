# devtool
1. composer require we7/devtool v0.1.5
2. bootstrap.inc.php 第一行加入 
```
include IA_ROOT.'/vendor/autoload.php';
``` 
3. bootstrap.inc.php 最后一行加入 
```
we7debugbar();
```
4. db.class.php 修改
```
$pdo = new $dbclass($dsn, $cfg['username'], $cfg['password'], $options);
		if(DEVELOPMENT && class_exists('\We7\Dev\DebugBar\W7\W7TraceablePDO')) {
			$pdo = new \We7\Dev\DebugBar\W7\W7TraceablePDO($pdo);
		}
		
```