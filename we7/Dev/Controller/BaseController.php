<?php
/**
 * [WeEngine System] Copyright (c) 2013 WE7.CC
 * User: fanyk
 * Date: 2018/2/9
 * Time: 16:13
 */

namespace We7\Dev\Controller;


use We7\Dev\DebugBar\We7DebugBar;

class BaseController {

	protected $debugbar;

	public function __construct() {
		$this->debugbar = We7DebugBar::instance();
	}
}