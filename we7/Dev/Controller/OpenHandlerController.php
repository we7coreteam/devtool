<?php namespace We7\Dev\Controller;


use DebugBar\OpenHandler;



class OpenHandlerController extends BaseController
{

    public function handle()
    {
        $openHandler = new OpenHandler($this->debugbar);
        $openHandler->handle(null, true, true);
    }

    /**
     * Return Clockwork output
     *
     * @param $id
     * @return mixed
     * @throws \DebugBar\DebugBarException
     */
    public function clockwork($id)
    {
        $request = [
            'op' => 'get',
            'id' => $id,
        ];

        $openHandler = new OpenHandler($this->debugbar);
        $data = $openHandler->handle($request, false, false);

        // Convert to Clockwork
        $converter = new Converter();
        $output = $converter->convert(json_decode($data, true));

        return response()->json($output);
    }
}
