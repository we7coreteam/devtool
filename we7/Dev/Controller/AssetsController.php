<?php
/**
 * [WeEngine System] Copyright (c) 2013 WE7.CC
 * User: fanyk
 * Date: 2018/2/9
 * Time: 15:52
 */

namespace We7\Dev\Controller;


class AssetsController extends BaseController {


	public function js() {
		$renderer = $this->debugbar->getJavascriptRenderer();
		$content = $renderer->dumpAssetsToString('js');

		header('Content-type : text/javascript');
		echo $content;
	}

	public function css() {

		$renderer = $this->debugbar->getJavascriptRenderer();
		$content = $renderer->dumpAssetsToString('css');
		header('Content-type : text/css');
		echo $content;
	}
}