<?php
/**
 * [WeEngine System] Copyright (c) 2013 WE7.CC
 * User: fanyk
 * Date: 2017/8/29
 * Time: 18:23.
 */
use We7\Dev\DebugBar\We7DebugBar;

if (!function_exists('dd')) {
	function dd($args) {
		dump($args);
		die(1);
	}
}

if (!function_exists('we7debug')) {
	function we7debug($showbar = true, $show_error_view = true, $storage_path = null) {
		//显示debugbar
		if (!$storage_path) {
			$storage_path = IA_ROOT.'/data/debugbar/';
		}
		if ($showbar) {
			$debugbar = We7DebugBar::instance($storage_path);
			$debugbar->showOnShutdown();
		}

		if ($show_error_view) {
			$whoops = new \Whoops\Run();
			$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
			$whoops->register();
		}
	}
}

if (!function_exists('we7debugbar')) {
	function we7debugbar($storage_path = null) {
		$debugbar = We7DebugBar::instance($storage_path);
		$debugbar->showOnShutdown();
		return $debugbar;
	}
}


if (!function_exists('we7error')) {
	function we7error() {
		$whoops = new \Whoops\Run();
		$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
		$whoops->register();
	}
}



if (!function_exists('storage_path')) {
	function storage_path() {
		return We7DebugBar::instance()->getStoragePath();
	}
}

if (!function_exists('base_path')) {
	function base_path() {
		return '/';
	}

}
