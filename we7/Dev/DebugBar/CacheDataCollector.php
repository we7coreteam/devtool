<?php

namespace We7\Dev\DebugBar;

use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;

class CacheDataCollector extends RequestDataCollector //extends DataCollector implements Renderable
{
	/**
	 * @return array
	 */
	public function collect() {
		return $this->cache_search();
	}

	private function cache_search() {
		$sql = 'SELECT * FROM '.tablename('core_cache');
		$params = array();
		$rs = pdo_fetchall($sql, $params);
		$result = array();
		foreach ((array) $rs as $v) {
			$value = iunserializer($v['value']);
			$value = $this->getDataFormatter()->formatVar($value);
			$result[$v['key']] = $value;
		}

		return $result;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'cache';
	}

	/**
	 * @return array
	 */
	public function getWidgets() {
		return array(
			'cache' => array(
				'icon' => 'tags',
				'widget' => 'PhpDebugBar.Widgets.VariableListWidget',
				'map' => 'cache',
				'default' => '{}',
			),
		);
	}
}
