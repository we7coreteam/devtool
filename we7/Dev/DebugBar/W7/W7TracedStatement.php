<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/2/26
 * Time: 15:38
 */

namespace We7\Dev\DebugBar\W7;


use DebugBar\DataCollector\PDO\TracedStatement;

class W7TracedStatement extends TracedStatement
{
	public $source;
	public function __construct($sql, array $params = array(), $preparedId = null)
	{
		$this->collectDebugTrace();
		parent::__construct($sql, $params, $preparedId);

		
	}
	
	public function collectDebugTrace()
	{
		$source = $this->findSource();
		$this->source = $source;
	}

	public function getSource()
	{
		return $this->source;
	}

	/**
	 * Use a backtrace to search for the origins of the query.
	 *
	 * @return array
	 */
	protected function findSource()
	{
		$stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS | DEBUG_BACKTRACE_PROVIDE_OBJECT, 50);

		$sources = [];

		foreach ($stack as $index => $trace) {
			$sources[$trace['file']] = $trace['line'];//$trace['line'] ;
		}
//		var_export($sources);
		return $sources;
//		return array_filter($sources);
	}



}