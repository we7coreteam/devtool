<?php

namespace We7\Dev\DebugBar;

class RequestDataCollector extends \DebugBar\DataCollector\RequestDataCollector {
	/**
	 * @return array
	 */
	public function collect() {
		$vars = array('_W', '_GPC', '_GET', '_POST', '_SESSION', '_COOKIE', '_SERVER');
		$data = array();

		foreach ($vars as $var) {
			if (isset($GLOBALS[$var])) {
				$data['$'.$var] = $this->getDataFormatter()->formatVar($GLOBALS[$var]);
			}
		}
		$data['uniacid'] = isset($GLOBALS['_W']['uniacid']) ? $GLOBALS['_W']['uniacid'] : null;
		$data['acid'] = isset($GLOBALS['_W']['acid']) ? $GLOBALS['_W']['acid'] : null;
		$data['uid'] = isset($GLOBALS['_W']['acid']) ? $GLOBALS['_W']['uid'] : null;

		return $data;
	}
}
