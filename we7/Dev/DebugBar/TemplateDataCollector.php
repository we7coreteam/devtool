<?php

namespace We7\Dev\DebugBar;

use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;

class TemplateDataCollector extends RequestDataCollector //extends DataCollector implements Renderable
{
	/**
	 * @return array
	 */
	public function collect() {
		global $_W;
		$data = array();
		$data['tpl_compile'] = $this->getDataFormatter()->formatVar($_W['tpl_compile']);
		$data['tpl_source'] = $this->getDataFormatter()->formatVar($_W['tpl_source']);

		return $data;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'template';
	}

	/**
	 * @return array
	 */
	public function getWidgets() {
		return array(
			'template' => array(
				'icon' => 'tags',
				'widget' => 'PhpDebugBar.Widgets.VariableListWidget',
				'map' => 'template',
				'default' => '{}',
			),
		);
	}
}
